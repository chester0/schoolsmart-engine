/*
 * Defines actions to be taken by different routes in the SchoolSmart
 * engine. This file has been created to allow easier testing of the
 * behaviour of each route.
 *
 * By the Pseudocoders team:
 * Demetrios C, Jordan, James E.
 *
 */

var firebase = require('./node_modules/firebase');
var distance = require("./node_modules/google-distance");

module.exports = {
    //Query the guardian's phone for status and travel time
    //return status and estimated travel time
    homePost: function (req, res) {
        // kiosk sends kiosk_id, student_id (RFID)
        // get value once from database to check if kiosk id exists
        var db = firebase.database();
        var kioskIdRef = db.ref("/kiosks/" + req.body.kioskid);
        var childIdRef = db.ref("/users/" + req.body.rfid + "/guardians/").orderByKey();
        //var dbMessagesRef = db.ref("messages").orderByKey(); // todo use db messages to respond
        var guardianId;

        kioskIdRef.once("value").then(function (snapshot) {
            if (!(snapshot.exists())) {                       // kiosk not found in the db
                console.log('KIOSK ID:' + req.body.kioskid + 'does not exist in the database');
                var msg = {
                    'Tone': -1,
                    'ResponseRequired': false,
                    'Message': 'ERROR: KIOSK ID: ' + req.body.kioskid + ' NOT FOUND IN THE DATABASE' +
                    ' \nContact support',
                    'ButtonOneText': null,
                    'ButtonTwoText': null,
                    'ButtonThreeText': null
                };
                res.json(msg);
                return false;
            } else {                                           // found kiosk id in the db, now find the kids guardian
                childIdRef.once("value").then(function (snapshot) {
                    if (!(snapshot.exists())) {                // RFID not found in the db
                        console.log('CHILD RFID: ' + req.body.rfid + ' does not exist in the database');
                        var msg = {
                            'Tone': -1,
                            'ResponseRequired': false,
                            'Message': 'ERROR: CHILD RFID: ' + req.body.rfid + ' NOT FOUND IN THE DATABASE' +
                            ' \nContact support',
                            'ButtonOneText': null,
                            'ButtonTwoText': null,
                            'ButtonThreeText': null
                        };
                        res.json(msg);
                        return false;
                    } else {                                    // rfid exists, get guardians key
                        var date = new Date();
                        var weekday = new Array(7);
                        weekday[0]=  "Sunday"; weekday[1] = "Monday"; weekday[2] = "Tuesday";
                        weekday[3] = "Wednesday"; weekday[4] = "Thursday"; weekday[5] = "Friday";
                        weekday[6] = "Saturday";
                        var today_day = weekday[date.getDay()];
                        var dd = date.getDate();
                        var mm = date.getMonth()+1; // January is 0!
                        var yyyy = date.getFullYear();
                        var todayDate = "" + dd + mm + yyyy;

                        // check date, then day, then default
                        snapshot.forEach(function(childSnapshot) {      // loop trough guardians to find the right one
                            if (todayDate == childSnapshot.key) {      // found special case with date use this guardian
                                guardianId = childSnapshot.val();
                                return true;
                            }
                            else if (today_day == childSnapshot.key) {   // found special case with day use this guardian
                                guardianId = childSnapshot.val();
                                return true;
                            }
                            else if (childSnapshot.key == "default") {   // no special cases, use default guardian
                                guardianId = childSnapshot.val();
                                return true;
                            }
                        });
                    }
                    // error checks above - no need to check below
                    var guardianIdRef = db.ref("/users/" + guardianId + "/latestMessage/");
                    guardianIdRef.child("kioskId").set(req.body.kioskid);
                    guardianIdRef.child("status").set("new");
                    guardianIdRef.child("type").set("pickupMessage");
                });
            }
        });
        // get response
        // calculate the ETA from the kiosks coordinates
        var kioskMessagesRef = db.ref("/kiosks/" + req.body.kioskid);

        kioskMessagesRef.once("value").then(function (snapshot) {
            var kioskCoordinates = snapshot.val().coordinates;
            var kioskLatestMessageCoordinates = snapshot.val().latestMessage.coordinates;
            console.log("From " +
                kioskLatestMessageCoordinates.latitude + ", " +
                kioskLatestMessageCoordinates.longitude + " to " +
                kioskCoordinates.latitude + ", " +
                kioskCoordinates.longitude);
            distance
                .get({
                    origin: kioskLatestMessageCoordinates.latitude + ", " + kioskLatestMessageCoordinates.longitude,
                    destination: kioskCoordinates.latitude + ", " + kioskCoordinates.longitude},
            function(err, data) {
                console.log(err);
                if (typeof(data) != "undefined" && typeof(data.duration) != "undefined") {
                    res.json({
                        'Tone': 1,
                        'ResponseRequired': false,
                        'Message': 'ETA: ' + data.duration,
                        'ButtonOneText': null,
                        'ButtonTwoText': null,
                        'ButtonThreeText': null
                    });
                } else {
                res.json({
                        'Tone': 1,
                        'ResponseRequired': false,
                        'Message': "Something went wrong",
                        'ButtonOneText': null,
                        'ButtonTwoText': null,
                        'ButtonThreeText': null
                    });
                }
            });
        })
    }
};
