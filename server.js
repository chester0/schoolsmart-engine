/* * A server for responding to kiosk requests for guardian ETA
 * Uses HTTP POST Kiosk side
 * Firebase client side
 * --------------------------
 * By the Pseudocoders team: 
 * Demetrios C, Jordan, James E.
 * */

var express = require('./node_modules/express');
var timeout = require('./node_modules/timeout');
var firebase = require('./node_modules/firebase');
var routes = require('./routes.js');
var app = express();
var constants = require('./constants');
var bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

// database connection
var config = {
    apiKey: "AIzaSyBigteMmTyAPrNdPUsIukR_GhYhIQI9Bhg",
    authDomain: "schoolsmart-3354c.firebaseapp.com",
    databaseURL: "https://schoolsmart-3354c.firebaseio.com",
    storageBucket: "schoolsmart-3354c.appspot.com"
};
firebase.initializeApp(config);
firebase.database().goOnline();
firebase.database.enableLogging(true);

// HTTP POST
app.post('/', routes.homePost);


/* *
 * Listen to $PORT for messages from the kiosk
 * */
app.listen(constants.PORT, function() {
    console.log('listening on port: ', constants.PORT);
    });

app.use(timeoutCallback); // THIS MUST COME LAST

// Callback when a request times out
function timeoutCallback(res) {
    // As the request has timed out, send an unknown status and no
    // travel time
    var msg = {status: 'timeout'};
    res.json(msg);
}