# README #

### SCHOOLSMART ENGINE ###

* Quick summary
This is the engine component of the SchoolSmart App
Communicates via HTTP POST to the Kiosk
JSON format
firebase is used to communicate to other clients: web portal and app

* Version 0.1 MVP Pseudocoders handoff

### How do I get set up? ###
Using WebStorm https://www.jetbrains.com/webstorm/


- open/import the "engine" directory from WebStorm
- Run -> Edit configurations
- Add NodeJS with javasctipt file: server.js, close dialog.

important! install dependencies prior running/changing
right click package.json -> run npm update
---
currently listening on port 8086 (edit constants.js to change)
Postman app https://www.getpostman.com/
can be used to simulate HTTP POST JSON requests to emulate the kiosk
---
Postman usage:

Type: POST
Headers:
Content-Type - application/json

Body: type is x-www-form-urlencoded
fields: kioskid, rfid
---
* Command line setup
npm install to install prerequisites
node server.js to run

* Dependencies
nodejs, firebase, express

* Database configuration
data is structured in json format
relationships are handled with duplicating a field with the true value to it
one can get a snapshot of the/parts database
or watch a value for changes


* How to run tests
tests are run with bitbucket pipelines on push and are located in /test/* 

* Deployment instructions
deploys with firebase atm:
terminal to dir
firebase deploy

### TODO ###
- write tests!!!
- fix bugs using the tests!!
- use database messages to respond
- check if parent is in geozone and customise msg accordingly
- hide database api key in a config file
- change database.rules.json permissions (currently read+write is enabled for all!)

### Who do I talk to? ###

Pseudocoders team: 
Demetrios Christou (chester0@gmail.com, dchr12@student.monash.edu)
Jordan,
James E.